<?php

namespace Inchoo\ShopReview\Ui\Component\Form\Element;

use Magento\Ui\Component\Form\Element\RadioSet;
use Inchoo\ShopReview\Helper\Data as RatingSource;
use Magento\Framework\Option\ArrayInterface;

/**
 * Class Rating
 * @package Inchoo\ShopReview\Ui\Component\Form\Element
 */
class Rating extends RadioSet implements ArrayInterface
{

    /**
     * @var RatingSource
     */
    protected $ratingSource;

    /**
     * Rating constructor.
     * @param RatingSource $ratingSource
     */
    public function __construct(
        RatingSource $ratingSource
    ) {
        $this->ratingSource = $ratingSource;
    }


    /**
     * Returns ratings as options array
     * @return array
     */
    public function toOptionArray()
    {
        return $this->ratingSource->getReviewRatingsOptionArray();
    }

}
