<?php
namespace Inchoo\ShopReview\Ui\Component\Form;

use Inchoo\ShopReview\Model\ResourceModel\Review\CollectionFactory;
use Magento\Ui\DataProvider\AbstractDataProvider;

/**
 * Class DataProvider
 * @package Inchoo\ShopReview\Ui\Component\Form
 */
class DataProvider extends AbstractDataProvider
{

    /**
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param \Inchoo\ShopReview\Model\ResourceModel\Review\CollectionFactory $collectionFactory
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $collectionFactory,
        array $meta = [],
        array $data = []
    ) {
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->collection = $collectionFactory->create();
    }


    /**
     * @return array
     */
    public function getData()
    {
        $data = [];
        $dataObject = $this->getCollection()->joinCustomers()->getFirstItem();
        if($dataObject->getId()) {
            $data[$dataObject->getId()] = $dataObject->toArray();
        }

        return $data;
    }
}