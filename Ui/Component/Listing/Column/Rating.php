<?php

namespace Inchoo\ShopReview\Ui\Component\Listing\Column;

use Magento\Framework\Data\OptionSourceInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Inchoo\ShopReview\Helper\Data as RatingSource;

/**
 * Class Rating
 * @package Inchoo\ShopReview\Ui\Component\Listing\Column
 */
class Rating extends Column implements OptionSourceInterface
{

    /**
     * @var RatingSource
     */
    protected $ratingSource;

    /**
     * Rating constructor.
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param RatingSource $ratingSource
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        RatingSource $ratingSource,
        array $components = [],
        array $data = []
    ) {
        parent::__construct($context, $uiComponentFactory, $components, $data);
        $this->ratingSource = $ratingSource;
    }

    /**
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        $dataSource = parent::prepareDataSource($dataSource);
        $options = $this->ratingSource->getReviewRatings();

        if (empty($dataSource['data']['items'])) {
            return $dataSource;
        }

        foreach ($dataSource['data']['items'] as &$item) {
            if (isset($options[$item['rating']])) {
                $item['rating'] = $options[$item['rating']];
            }
        }


        return $dataSource;
    }


    /**
     * @return array
     */
    public function toOptionArray()
    {
        return $this->ratingSource->getReviewRatingsOptionArray();
    }
}
