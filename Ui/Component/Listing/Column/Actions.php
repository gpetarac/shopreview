<?php
namespace Inchoo\ShopReview\Ui\Component\Listing\Column;
use Magento\Ui\Component\Listing\Columns\Column;

/**
 * Class Actions
 * @package Inchoo\ShopReview\Ui\Component\Listing\Column
 */
class Actions extends Column
{
    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (!isset($dataSource['data']['items'])) {
            return $dataSource;
        }

        foreach ($dataSource['data']['items'] as & $item) {
            if (isset($item['review_id'])) {
                $item[$this->getData('name')] = [
                    'edit' => [
                        'href' => $this->context->getUrl(
                            'shop_review/review/create',
                            ['review_id' => $item['review_id']]
                        ),
                        'label' => __('Edit')
                    ],

                    'delete' => [
                        'href' => $this->context->getUrl(
                            'shop_review/review/delete',
                            ['review_id' => $item['review_id']]
                        ),
                        'label' => __('Delete')
                    ]

                ];
            }
        }

        return $dataSource;
    }
}