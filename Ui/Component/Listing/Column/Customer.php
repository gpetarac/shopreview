<?php

namespace Inchoo\ShopReview\Ui\Component\Listing\Column;

use Magento\Ui\Component\Listing\Columns\Column;

/**
 * Class Customer
 * @package Inchoo\ShopReview\Ui\Component\Listing\Column
 */
class Customer extends Column
{

    /**
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        $dataSource = parent::prepareDataSource($dataSource);

        if (empty($dataSource['data']['items'])) {
            return $dataSource;
        }

        foreach ($dataSource['data']['items'] as &$item) {
            $item['customer'] = $item['customer_real_name'] ?? $item['customer_name'];
        }

        return $dataSource;
    }


}
