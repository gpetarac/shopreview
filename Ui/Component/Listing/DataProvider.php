<?php
namespace Inchoo\ShopReview\Ui\Component\Listing;

use Magento\Ui\DataProvider\AbstractDataProvider;
use Inchoo\ShopReview\Model\ResourceModel\Review\CollectionFactory;

/**
 * Class DataProvider
 * @package Inchoo\ShopReview\Ui\Component\Listing
 */
class DataProvider extends AbstractDataProvider
{
    /**
     * DataProvider constructor.
     * @param $name
     * @param $primaryFieldName
     * @param $requestFieldName
     * @param CollectionFactory $collectionFactory
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $collectionFactory,
        array $meta = [],
        array $data = []
    ) {
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->collection = $collectionFactory->create();
    }
    /**
     *
     * {@inheritdoc}
     */
    public function getData()
    {

        $data = $this->getCollection()->joinCustomers()->toArray();
        return $data;
    }
}