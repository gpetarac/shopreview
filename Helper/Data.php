<?php

namespace Inchoo\ShopReview\Helper;

/**
 * Default review helper
 * Class Data
 * @package Inchoo\ShopReview\Helper
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{

    /**
     * Filter manager
     *
     * @var \Magento\Framework\Filter\FilterManager
     */
    protected $filter;

    /**
     * Escaper
     *
     * @var \Magento\Framework\Escaper
     */
    protected $_escaper;

    /**
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Framework\Escaper $escaper
     * @param \Magento\Framework\Filter\FilterManager $filter
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\Escaper $escaper,
        \Magento\Framework\Filter\FilterManager $filter
    ) {
        $this->_escaper = $escaper;
        $this->filter = $filter;
        parent::__construct($context);
    }


    /**
     * Get review statuses
     *
     * @return array
     */
    public function getReviewStatuses()
    {
        return [
            \Inchoo\ShopReview\Model\Review::STATUS_APPROVED => __('Approved'),
            \Inchoo\ShopReview\Model\Review::STATUS_PENDING => __('Pending'),
            \Inchoo\ShopReview\Model\Review::STATUS_NOT_APPROVED => __('Not Approved')
        ];
    }

    /**
     * Get review statuses as option array
     *
     * @return array
     */
    public function getReviewStatusesOptionArray()
    {
        $result = [];
        foreach ($this->getReviewStatuses() as $value => $label) {
            $result[] = ['value' => $value, 'label' => $label];
        }

        return $result;
    }


    /**
     * Get review ratings
     *
     * @return array
     */
    public function getReviewRatings()
    {
        return [
            20 => __('1 star'),
            40 => __('2 stars'),
            60 => __('3 stars'),
            80 => __('4 stars'),
            100 => __('5 stars')
        ];
    }

    /**
     * Get review ratings as option array
     *
     * @return array
     */
    public function getReviewRatingsOptionArray()
    {
        $result = [];
        foreach ($this->getReviewRatings() as $value => $label) {
            $result[] = ['value' => $value, 'label' => $label];
        }


        return $result;
    }





}
