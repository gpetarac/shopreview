<?php

namespace Inchoo\ShopReview\Block\Review;

use Inchoo\ShopReview\Model\Review;

/**
 * Class Index
 * @package Inchoo\ShopReview\Block\Review
 */
class Index extends \Inchoo\ShopReview\Block\Review
{

    protected $data = array();

    /**
     * Returns array of urls based on review (crud)
     * @param Review|null $review
     * @return array
     */
    public function getUrlStrategy(Review $review = null)
    {
        if(!$review->getId()){

            $this->data[$this->getCreateUrl()] = __('Create Review');

        }else{

            $this->data[$this->getEditUrl()] = __('Edit Review');
            $this->data[$this->getDeleteUrl()] = __('Delete Review');
        }

        $this->data[$this->getBackUrl()] = __('Back');

        return $this->data;

    }

    /**
     * @return string
     */
    public function getCreateUrl()
    {
        return $this->getUrl('shop_review/review/create/');
    }


    /**
     * @return string
     */
    public function getEditUrl()
    {
        return $this->getUrl('shop_review/review/edit');
    }


    /**
     * @return string
     */
    public function getDeleteUrl()
    {
        return $this->getUrl('shop_review/review/delete');
    }


    /**
     * Return review customer url
     *
     * @return string
     */
    public function getBackUrl()
    {
        return $this->getUrl('shop_review/customer');
    }





}
