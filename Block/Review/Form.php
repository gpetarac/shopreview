<?php

namespace Inchoo\ShopReview\Block\Review;


/**
 * Class Form
 * @package Inchoo\ShopReview\Block\Review
 */
class Form extends \Inchoo\ShopReview\Block\Review
{

    /**
     * Get review post action
     *
     * @return string
     */
    public function getAction()
    {
        return $this->getUrl(
            'shop_review/review/save',
            [
                '_secure' => $this->getRequest()->isSecure()
            ]
        );
    }

}
