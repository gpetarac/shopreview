<?php

namespace Inchoo\ShopReview\Block\Review;

/**
 * Class View
 * @package Inchoo\ShopReview\Block\Review
 */
class View extends \Inchoo\ShopReview\Block\Review
{

    /**
     * @return bool|\Magento\Framework\DataObject[]
     */
    public function getReviews()
    {
        $isEnabled = $this->_scopeConfig->getValue('inchoo_shop_review/general/enable');

        if(!$isEnabled){

            return false;
        }

        $this->_collection = $this->_collectionFactory->create();

        return $this->_collection
            ->joinStore($this->context->getStoreManager()->getStore()->getId())
            ->addStatusFilter(\Inchoo\ShopReview\Model\Review::STATUS_APPROVED)
            ->joinCustomers()
            ->getItems();

    }

}
