<?php

namespace Inchoo\ShopReview\Block;

use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\Registry;
use Inchoo\ShopReview\Model\ResourceModel\Review\Collection;
use Inchoo\ShopReview\Model\ResourceModel\Review\CollectionFactory;

/**
 * Class Review
 * @package Inchoo\ShopReview\Block
 */
class Review extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Magento\Framework\View\Element\Template\Context $context
     */
    protected $context;

    /**
     * @var \Magento\Framework\Registry
     */

    protected $_registry;


    /**
     * Shop review collection
     *
     * @var \Inchoo\ShopReview\Model\ResourceModel\Review\Collection
     */
    protected $_collection;

    /**
     * Review resource model
     *
     * @var \Inchoo\ShopReview\Model\ResourceModel\Review\CollectionFactory
     */
    protected $_collectionFactory;

    /**
     * Review constructor.
     * @param Context $context
     */
    public function __construct(
        Context $context,
        Registry $_registry,
        CollectionFactory $collectionFactory,
        Collection $collection
    ) {

        parent::__construct($context);
        $this->context = $context;
        $this->_registry = $_registry;
        $this->_collectionFactory = $collectionFactory;
        $this->_collection = $collection;
    }



    /**
     * Format date in short format
     *
     * @param string $date
     * @return string
     */
    public function dateFormat($date)
    {
        return $this->formatDate($date, \IntlDateFormatter::SHORT, true);
    }



    /**
     * Retrieves review object form registry
     * @return mixed
     */
    public function getReview()
    {
        return $this->_registry->registry('shop_review');

    }




}
