<?php
namespace Inchoo\ShopReview\Model\ResourceModel\Review;

/**
 * Class Collection
 * @package Inchoo\ShopReview\Model\ResourceModel\Review
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * Review data
     *
     * @var \Inchoo\ShopReview\Helper\Data
     */
    protected $_reviewData = null;

    /**
     * Initialize review Collection
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Inchoo\ShopReview\Model\Review::class,
            \Inchoo\ShopReview\Model\ResourceModel\Review::class
        );
    }


    /**
     * Filters customer by given id
     * @param $customerId
     * @return $this
     */
    public function addCustomerFilter($customerId)
    {
        $this->addFieldToFilter('customer_id', $customerId);

        return $this;
    }


    /**
     * Add status filter
     *
     * @param int|string $status
     * @return $this
     */
    public function addStatusFilter($status)
    {
        $this->addFieldToFilter('status', $status);

        return $this;
    }


    /**
     * Add store filter
     *
     * @param $storeId
     * @return $this
     */
    public function addStoreFilter($storeId)
    {
        $this->addFieldToFilter('store_id', $storeId);

        return $this;
    }


    /**
     * Join customers table
     *
     * @return $this
     */
    public function joinCustomers()
    {
        $this->getSelect()->joinLeft(
            ['customer' => $this->_resource->getTable('customer_entity')],
            'main_table.customer_id = customer.entity_id',
            ['customer_real_name' => "CONCAT(firstname, ' ', lastname)"]
        );


        return $this;
    }


    /**
     * Join store table
     *
     * @return $this
     */
    public function joinStore($storeId)
    {
        $this->getSelect()->joinLeft(
            ['store' => $this->_resource->getTable('store')],
            'main_table.store_id = store.store_id',
            []
        );

        $this->addFieldToFilter('main_table.store_id', $storeId);


        return $this;
    }




}