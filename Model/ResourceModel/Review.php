<?php
namespace Inchoo\ShopReview\Model\ResourceModel;
/**
 * Class Review
 * @package Inchoo\ShopReview\Model\ResourceModel
 */
class Review extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize review Resource
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('inchoo_shop_review', 'review_id');
    }


}