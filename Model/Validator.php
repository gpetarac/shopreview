<?php
namespace Inchoo\ShopReview\Model;

use Zend_Validate_Exception;
use Magento\Framework\Data\Form\FormKey\Validator As FormKeyValidator;

class Validator extends \Magento\Framework\Validator\AbstractValidator
{

    /**
     * @var \Magento\Framework\Data\Form\FormKey\Validator
     */
    protected $formKeyValidator;


    /**
     * Validator constructor.
     * @param FormKeyValidator $formKeyValidator
     */
    public function __construct(FormKeyValidator $formKeyValidator)
    {
        $this->formKeyValidator = $formKeyValidator;
    }


    /**
     * @param mixed $request
     * @return bool|void
     * @throws Zend_Validate_Exception
     */
    public function isValid($request)
    {
        if (!$request->getParam('rating') || !\Zend_Validate::is(trim($request->getParam('rating')), 'NotEmpty')) {
            throw new \Zend_Validate_Exception(__('Rating is missing'));
        }

        if (!\Zend_Validate::is(trim($request->getParam('title')), 'NotEmpty')) {
            throw new \Zend_Validate_Exception(__('Title is missing'));
        }

        if (!\Zend_Validate::is(trim($request->getParam('description')), 'NotEmpty')) {
            throw new \Zend_Validate_Exception(__('Description is missing'));
        }

        if(!$this->formKeyValidator->validate($request)){
            throw new \Zend_Validate_Exception(__('Something went wrong with saving. Please try again'));
        }
    }




}