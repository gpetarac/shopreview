<?php
namespace Inchoo\ShopReview\Model;

/**
 * Review model
 *
 * @method string getCustomerRealName()
 * @method string getCustomerName()
 */
class Review extends \Magento\Framework\Model\AbstractModel
{
    /**
     * Approved review status code
     */
    const STATUS_APPROVED = 'Approved';

    /**
     * Pending review status code
     */
    const STATUS_PENDING = 'Pending';

    /**
     * Not Approved review status code
     */
    const STATUS_NOT_APPROVED = 'Not approved';

    /**
     * Initialize review Model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Inchoo\ShopReview\Model\ResourceModel\Review::class);
    }


    /**
     * Returns either real customer or fake customer name
     * @return string
     */
    public function getCustomerFullName()
    {

        if($this->getCustomerRealName()){
            return $this->getCustomerRealName();
        }

        return $this->getCustomerName();
    }

}