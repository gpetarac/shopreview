<?php
namespace Inchoo\ShopReview\Setup;
use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Setup\Model\DateTime\DateTimeProvider;

class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * {@inheritdoc}
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $shopReviewTable = $setup->getTable('inchoo_shop_review');
        $customerEntityTable = $setup->getTable('customer_entity');
        $storeTable = $setup->getTable('store');


        $setup->getConnection()->addForeignKey(
            $setup->getFkName($shopReviewTable, 'customer_id', $customerEntityTable, 'entity_id'),
            $shopReviewTable,
            'customer_id',
            $customerEntityTable,
            'entity_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        );


        $setup->getConnection()->addForeignKey(
            $setup->getFkName($shopReviewTable, 'store_id', $storeTable, 'store_id'),
            $shopReviewTable,
            'store_id',
            $storeTable,
            'store_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        );

        $setup->endSetup();
    }
}