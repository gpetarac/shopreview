<?php

namespace Inchoo\ShopReview\Controller\Review;

use Inchoo\ShopReview\Controller\Review as ReviewController;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class Delete
 * @package Inchoo\ShopReview\Controller\Review
 */
class Delete extends ReviewController
{

    /**
     * @return $this|\Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();

        try {

            /** @var \Inchoo\ShopReview\Model\Review $review */
            $review = $this->getReview();

            $this->validateDelete($review);

            $this->reviewResource->delete($review);

            $this->messageManager->addSuccessMessage(__('Review successfully deleted'));


        } catch (\Exception $e) {

            $this->messageManager->addExceptionMessage($e, __($e->getMessage()));
        }

        return $resultRedirect->setPath('*/*/');

    }


    /**
     * @param \Inchoo\ShopReview\Model\Review $review
     * @throws NoSuchEntityException
     */
    private function validateDelete(\Inchoo\ShopReview\Model\Review $review){

        if(!$review->getId()){

            throw new NoSuchEntityException(__('Nothing to delete.'));

        }
    }


}
