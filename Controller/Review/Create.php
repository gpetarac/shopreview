<?php

namespace Inchoo\ShopReview\Controller\Review;

use Inchoo\ShopReview\Controller\Review as ReviewController;
use Magento\Framework\Controller\ResultFactory;

/**
 * Class Create
 * @package Inchoo\ShopReview\Controller\Review
 */
class Create extends ReviewController
{
    /**
     * Render form
     *
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        /** @var \Magento\Framework\View\Result\Page $resultPage */
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);

        if ($navigationBlock = $resultPage->getLayout()->getBlock('customer_account_navigation')) {
            $navigationBlock->setActive('shop_review/review');
        }

        $this->registerReview();
        $resultPage->getConfig()->getTitle()->set(__('Create Shop Review'));
        return $resultPage;
    }
}
