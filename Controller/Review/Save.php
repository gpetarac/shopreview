<?php

namespace Inchoo\ShopReview\Controller\Review;

use Inchoo\ShopReview\Controller\Review as ReviewController;
/**
 * Class Save
 * @package Inchoo\ShopReview\Controller\Review
 */
class Save extends ReviewController
{

    /**
     * @return $this|\Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();

        $request = $this->getRequest();

        try{

            $this->validator->isValid($request);

            $this->save($request->getPostValue());

        } catch (\Exception $e) {

            $this->messageManager->addExceptionMessage($e, __($e->getMessage()));
            return $resultRedirect->setUrl($this->_redirect->getRefererUrl());

        }


        return $resultRedirect->setPath('*/*/');

    }


    /**
     * @param array $reviewData
     * @throws \Exception
     * @throws \Magento\Framework\Exception\AlreadyExistsException
     */
    private function save(array $reviewData)
    {

        $review = $this->reviewModelFactory->create();

        // this check is for update, system cannot figure out itself
        if(!empty($reviewData['review_id'])){
            $review->setReviewId($reviewData['review_id']);
        }

        $review->setTitle($reviewData['title']);
        $review->setDescription($reviewData['description']);
        $review->setRating($reviewData['rating']);
        $review->setCustomerId($this->customerSession->getCustomerId());
        $review->setStoreId($this->storeManager->getStore()->getId());
        $review->setStatus(\Inchoo\ShopReview\Model\Review::STATUS_PENDING);

        $this->reviewResource->save($review);
        $this->messageManager->addSuccessMessage(__('Review has been saved'));

        /** setting email event */
        $this->setEmailEvent($review, !empty($reviewData['review_id']));
    }


    /**
     * Setting event for sending email to admin after form has been submitted
     * @param \Inchoo\ShopReview\Model\Review $review
     * @param bool $isUpdate
     */
    private function setEmailEvent(\Inchoo\ShopReview\Model\Review $review, bool $isUpdate)
    {

        if($this->customerSession->getCustomerId()) {
            $this->_eventManager->dispatch('shop_review_send_email', [
                    'review' => $review,
                    'customer' => $this->customerSession->getCustomer(),
                    'is_update' => $isUpdate
                ]
            );
        }

    }
}
