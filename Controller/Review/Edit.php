<?php

namespace Inchoo\ShopReview\Controller\Review;

use Inchoo\ShopReview\Controller\Review as ReviewController;
use Magento\Framework\Controller\ResultFactory;

/**
 * Class Edit
 * @package Inchoo\ShopReview\Controller\Review
 */
class Edit extends ReviewController
{
    /**
     * @return $this|\Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|\Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        /** @var \Magento\Framework\View\Result\Page $resultPage */
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();

        if ($navigationBlock = $resultPage->getLayout()->getBlock('customer_account_navigation')) {
            $navigationBlock->setActive('shop_review/review');
        }


        if(!$this->getReview()->getId()){
            return $resultRedirect->setPath('*/*/');
        }

        $this->registerReview();
        $resultPage->getConfig()->getTitle()->set(__('Edit Shop Review'));
        return $resultPage;
    }
}
