<?php

namespace Inchoo\ShopReview\Controller\Review;

use Inchoo\ShopReview\Controller\Review as ReviewController;
use Magento\Framework\Controller\ResultFactory;

/**
 * Class Index
 * @package Inchoo\ShopReview\Controller\Review
 */
class Index extends ReviewController
{
    /**
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        /** @var \Magento\Framework\View\Result\Page $resultPage */
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $resultPage->getConfig()->getTitle()->set(__('My Shop Review'));
        $this->registerReview();
        return $resultPage;
    }


}
