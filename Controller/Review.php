<?php

namespace Inchoo\ShopReview\Controller;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Customer\Model\Session;
use Magento\Framework\App\RequestInterface;
use Inchoo\ShopReview\Model\ResourceModel\Review\Collection;
use Inchoo\ShopReview\Model\ResourceModel\Review\CollectionFactory;
use Inchoo\ShopReview\Model\ResourceModel\Review As ReviewResource;
use Inchoo\ShopReview\Model\ReviewFactory;
use Magento\Framework\Registry;
use Magento\Store\Model\StoreManagerInterface As StoreManager;
use Inchoo\ShopReview\Model\Validator;


/**
 * Class Review
 * @package Inchoo\ShopReview\Controller
 */
abstract class Review extends Action
{

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;


    /**
     * Customer session model
     *
     * @var \Magento\Customer\Model\Session
     */
    protected $customerSession;


    /**
     * Shop review collection
     *
     * @var \Inchoo\ShopReview\Model\ResourceModel\Review\Collection
     */
    protected $_collection;

    /**
     * Review resource model
     *
     * @var \Inchoo\ShopReview\Model\ResourceModel\Review\CollectionFactory
     */
    protected $_collectionFactory;


    /**
     * @var \Magento\Framework\View\Element\Template\Context $context
     */
    protected $context;


    /**
     * @var \Inchoo\ShopReview\Model\ResourceModel\Review
     */
    protected $reviewResource;


    /**
     * @var \Inchoo\ShopReview\Model\ReviewFactory
     */
    protected $reviewModelFactory;


    /**
     * @var \Magento\Framework\Registry
     */

    protected $_registry;


    /**
     * @var \Inchoo\ShopReview\Model\Validator
     */
    protected $validator;


    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Customer\Model\Session $customerSession
     */
    public function __construct(
        Context $context,
        Session $customerSession,
        CollectionFactory $collectionFactory,
        Collection $collection,
        ReviewResource $reviewResource,
        ReviewFactory $reviewModelFactory,
        Registry $registry,
        StoreManager $storeManager,
        Validator $validator
    ) {
        $this->customerSession = $customerSession;
        $this->_collectionFactory = $collectionFactory;
        $this->_collection = $collection;
        $this->reviewModelFactory = $reviewModelFactory;
        $this->reviewResource = $reviewResource;
        $this->context = $context;
        $this->storeManager = $storeManager;
        $this->_registry = $registry;
        $this->validator = $validator;
        parent::__construct($context);
    }

    /**
     * @param RequestInterface $request
     * @return \Magento\Framework\App\ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function dispatch(RequestInterface $request)
    {

        if (!$this->customerSession->authenticate()) {
            $this->_actionFlag->set('', self::FLAG_NO_DISPATCH, true);
        }

        return parent::dispatch($request);
    }


    /**
     * Returns single customer review per store
     * @return \Magento\Framework\DataObject
     */
    protected function getReview()
    {

        $this->_collection = $this->_collectionFactory->create();

        return $this->_collection
            ->addStoreFilter($this->storeManager->getStore()->getId())
            ->addCustomerFilter($this->customerSession->getCustomerId())
            ->getFirstItem();

    }

    /**
     * Setting review object to register
     */
    protected function registerReview()
    {
        $this->_registry->register('shop_review', $this->getReview());

    }
}
