<?php
namespace Inchoo\ShopReview\Controller\Adminhtml\Review;
use Magento\Framework\Controller\ResultFactory;
use Inchoo\ShopReview\Controller\Adminhtml\Review as ReviewController;

/**
 * Class Create
 * @package Inchoo\ShopReview\Controller\Adminhtml\Review
 */
class Create extends ReviewController
{
    /**
     * @return \Magento\Backend\Model\View\Result\Page|\Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $reviewId = $this->getRequest()->getParam('review_id');

        $title = $reviewId ? __('Edit Review') : __('Create Review');

        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $resultPage->setActiveMenu('Inchoo_ShopReview::review');
        $resultPage->getConfig()->getTitle()->prepend(__($title));
        return $resultPage;
    }
}