<?php
namespace Inchoo\ShopReview\Controller\Adminhtml\Review;

use Inchoo\ShopReview\Controller\Adminhtml\Review as ReviewController;
use Magento\Backend\App\Action\Context;
use Inchoo\ShopReview\Model\ResourceModel\Review As ReviewResource;
use Inchoo\ShopReview\Model\ReviewFactory;

/**
 * Class Delete
 * @package Inchoo\ShopReview\Controller\Adminhtml\Review
 */
class Delete extends ReviewController
{

    /**
     * @var \Magento\Backend\App\Action\Context; $context
     */
    protected $context;


    /**
     * @var \Inchoo\ShopReview\Model\ResourceModel\Review
     */
    protected $reviewResource;


    /**
     * @var \Inchoo\ShopReview\Model\ReviewFactory
     */
    protected $reviewModelFactory;


    /**
     * Save constructor.
     * @param Context $context
     * @param ReviewResource $reviewResource
     * @param ReviewFactory $reviewModelFactory
     */
    public function __construct(
        Context $context,
        ReviewResource $reviewResource,
        ReviewFactory $reviewModelFactory

    ) {
        $this->reviewModelFactory = $reviewModelFactory;
        $this->reviewResource = $reviewResource;
        $this->context = $context;;
        parent::__construct($context);
    }


    /**
     * @return $this|\Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();

        try {

            $reviewId = $this->getRequest()->getParam('review_id');

            $review = $this->reviewModelFactory->create();
            $this->reviewResource->load($review, $reviewId);
            $this->reviewResource->delete($review);

            $this->messageManager->addSuccessMessage(__('Review has been deleted'));

        } catch (\Exception $e) {

            $this->messageManager->addExceptionMessage($e, __($e->getMessage()));
            return $resultRedirect->setUrl($this->_redirect->getRefererUrl());

        }

        return $resultRedirect->setPath('*/*/');

    }
}