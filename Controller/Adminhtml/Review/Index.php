<?php
namespace Inchoo\ShopReview\Controller\Adminhtml\Review;
use Magento\Framework\Controller\ResultFactory;
use Inchoo\ShopReview\Controller\Adminhtml\Review as ReviewController;

/**
 * Class Index
 * @package Inchoo\ShopReview\Controller\Adminhtml\Review
 */
class Index extends ReviewController
{
    /**
     * Index action
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $resultPage->setActiveMenu('Inchoo_ShopReview::review');
        $resultPage->getConfig()->getTitle()->prepend(__('Shop Review'));
        return $resultPage;
    }
}