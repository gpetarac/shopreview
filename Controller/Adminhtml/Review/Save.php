<?php
namespace Inchoo\ShopReview\Controller\Adminhtml\Review;

use Inchoo\ShopReview\Controller\Adminhtml\Review as ReviewController;
use Magento\Backend\App\Action\Context;
use Inchoo\ShopReview\Model\ResourceModel\Review As ReviewResource;
use Inchoo\ShopReview\Model\ReviewFactory;

/**
 * Class Save
 * @package Inchoo\ShopReview\Controller\Adminhtml\Review
 */
class Save extends ReviewController
{

    /**
     * @var \Magento\Backend\App\Action\Context; $context
     */
    protected $context;


    /**
     * @var \Inchoo\ShopReview\Model\ResourceModel\Review
     */
    protected $reviewResource;


    /**
     * @var \Inchoo\ShopReview\Model\ReviewFactory
     */
    protected $reviewModelFactory;


    /**
     * Save constructor.
     * @param Context $context
     * @param ReviewResource $reviewResource
     * @param ReviewFactory $reviewModelFactory
     */
    public function __construct(
        Context $context,
        ReviewResource $reviewResource,
        ReviewFactory $reviewModelFactory

    ) {
        $this->reviewModelFactory = $reviewModelFactory;
        $this->reviewResource = $reviewResource;
        $this->context = $context;;
        parent::__construct($context);
    }


    /**
     * Save action
     * @return $this|\Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();


        $data = $this->getRequest()->getPostValue();

        if(!$data){

            return $resultRedirect->setPath('*/*/');
        }

        // this check is for update, system cannot figure out itself
        if (empty($data['review_id'])) {
            unset($data['review_id']);
        }

        try {

            $review = $this->reviewModelFactory->create();
            $review->setData($data);
            $this->reviewResource->save($review);

            $this->messageManager->addSuccessMessage(__('Review has been saved'));


        } catch (\Exception $e) {

            $this->messageManager->addExceptionMessage($e, __($e->getMessage()));
            return $resultRedirect->setUrl($this->_redirect->getRefererUrl());

        }


        return $resultRedirect->setPath('*/*/');

    }
}