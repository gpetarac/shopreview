<?php

namespace Inchoo\ShopReview\Controller\Adminhtml;

/**
 * Class Review
 * @package Inchoo\ShopReview\Controller\Adminhtml
 */
abstract class Review  extends \Magento\Backend\App\Action
{
    /**
     * Check the permission to run it
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Inchoo_ShopReview::review');
    }

}
