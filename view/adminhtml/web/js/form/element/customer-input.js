define([
    'underscore',
    'uiRegistry',
    'Magento_Ui/js/form/element/abstract',
    'Magento_Ui/js/modal/modal'
], function (_, uiRegistry, abstract, modal) {
    'use strict';

    return abstract.extend({

        /**
         * Showing customer name field based on value
         * Gets initial value of element
         *
         * @returns {*} Elements' value.
         */
        getInitialValue: function () {

            var customerRealName = uiRegistry.get('index = customer_real_name').initialValue;
            var customerFakeName = this.value();

            if(typeof customerRealName !== 'undefined' && customerRealName){

                this.disable();
                return this.normalizeData(customerRealName);

            }else{

                return this.normalizeData(customerFakeName);

            }

        }

    });
});